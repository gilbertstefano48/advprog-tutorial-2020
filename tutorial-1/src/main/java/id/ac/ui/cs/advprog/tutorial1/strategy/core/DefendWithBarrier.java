package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithBarrier implements DefenseBehavior {
    public String defend(){
        return "Defends with Barrier";
    }
    public String getType(){
        return "Barrier";
    }
}
