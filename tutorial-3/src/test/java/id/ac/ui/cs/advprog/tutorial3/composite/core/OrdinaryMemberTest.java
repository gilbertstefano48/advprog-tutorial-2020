package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrdinaryMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new OrdinaryMember("Nina", "Merchant");
    }

    @Test
    public void testMethodGetName() {
        //TODO: Complete me
        assertEquals("Nina", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Merchant", member.getRole());
    }

    @Test
    public void testMethodAddRemoveChildMemberDoNothing() {
        //TODO: Complete me
        Member child1 = new OrdinaryMember("Algi","Tukang Ngoding");
        Member child2 = new OrdinaryMember("Algo","Implementasi Algi");

        member.addChildMember(child1);
        member.addChildMember(child2);
        assertEquals(0,member.getChildMembers().size());

        member.removeChildMember(child2);
        assertEquals(0,member.getChildMembers().size());
    }
}
