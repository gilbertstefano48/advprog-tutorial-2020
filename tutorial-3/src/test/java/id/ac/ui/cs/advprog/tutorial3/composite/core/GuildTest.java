package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        int before = guild.getMemberList().size();
        Member madara = new OrdinaryMember("Tsaqif", "Merchant");
        guild.addMember(guildMaster,madara);

        assertEquals(before+1, guild.getMemberList().size());
        assertEquals(madara, guild.getMember("Tsaqif","Merchant"));
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member itachi = new PremiumMember("Nevil","Sensei");
        guild.addMember(guildMaster,itachi);
        int before = guild.getMemberList().size();
        guild.removeMember(guildMaster,itachi);

        assertEquals(before-1, guild.getMemberList().size());
        assertEquals(null,guild.getMember("Nevil","Sensei"));
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member orochimaru = new OrdinaryMember("Aan","Priest");
        guild.addMember(guildMaster,orochimaru);
        assertEquals(orochimaru, guild.getMember("Aan","Priest"));
    }
}
