package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;
    private Member master;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
        master = new PremiumMember("Sutiyem", "Master");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant",member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member child = new OrdinaryMember("Ashoy","Tukang pukul");
        member.addChildMember(child);

        assertEquals(true, member.getChildMembers().contains(child));
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member child = new OrdinaryMember("Ashoy","Tukang pukul");
        member.addChildMember(child);
        member.removeChildMember(child);

        assertEquals(0,member.getChildMembers().size());
        assertEquals(false,member.getChildMembers().contains(member));
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member child1 = new OrdinaryMember("Ashoy","Tukang pukul");
        Member child2 = new OrdinaryMember("Asik","Tukang pukul");
        Member child3 = new OrdinaryMember("Santuy","Tukang pukul");
        Member child4 = new OrdinaryMember("Yogs","Tukang pukul");

        member.addChildMember(child1);
        member.addChildMember(child2);
        member.addChildMember(child3);
        member.addChildMember(child4);

        assertEquals(3,member.getChildMembers().size());
        assertEquals(false, member.getChildMembers().contains(child4));
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member child1 = new OrdinaryMember("Ashoy","Tukang pukul");
        Member child2 = new OrdinaryMember("Asik","Tukang pukul");
        Member child3 = new OrdinaryMember("Santuy","Tukang pukul");
        Member child4 = new OrdinaryMember("Yogs","Tukang pukul");

        master.addChildMember(child1);
        master.addChildMember(child2);
        master.addChildMember(child3);
        master.addChildMember(child4);

        assertEquals(4,master.getChildMembers().size());
        assertEquals(true, master.getChildMembers().contains(child4));
    }
}
