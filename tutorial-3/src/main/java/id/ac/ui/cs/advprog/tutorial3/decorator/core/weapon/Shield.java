package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Shield extends Weapon {
    //TODO: Complete me
    public Shield(){
        this.weaponName = "Shield";
        this.weaponValue = 10;
        this.weaponDescription = "Heater Shield";
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getWeaponValue() {
        return super.getWeaponValue();
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }
}
