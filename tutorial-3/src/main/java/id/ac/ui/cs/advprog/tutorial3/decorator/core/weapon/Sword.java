package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Sword extends Weapon {
    //TODO: Complete me
    public Sword(){
        this.weaponName = "Sword";
        this.weaponValue = 25;
        this.weaponDescription = "Great Sword";
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getWeaponValue() {
        return super.getWeaponValue();
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }
}
