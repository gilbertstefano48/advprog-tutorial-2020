package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Gun extends Weapon {
    //TODO: Complete me
    public Gun(){
        this.weaponName = "Gun";
        this.weaponValue = 20;
        this.weaponDescription = "Automatic Gun";
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getWeaponValue() {
        return super.getWeaponValue();
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }
}
