package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    //TODO: Complete me
    protected List<Member> childList = new ArrayList<>();
    protected String name, role;

    public PremiumMember(String name, String role){
        this.name = name;
        this.role = role;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {
        if (this.role.equals("Master"))
            this.childList.add(member);
        else {
            if (this.childList.size() >= 3) {}
            else this.childList.add(member);
        }
    }

    @Override
    public void removeChildMember(Member member) {
        this.childList.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return this.childList;
    }
}
