package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

public class Longbow extends Weapon {
    //TODO: Complete me
    public Longbow(){
        this.weaponName = "Longbow";
        this.weaponValue = 15;
        this.weaponDescription = "Big Longbow";
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public int getWeaponValue() {
        return super.getWeaponValue();
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }
}
